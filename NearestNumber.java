import java.util.Arrays;
public class NearestNumber{
    public static void main(String[] args) {
        int numbers[] = {5,8,9,6,4,1,2,3,0};
        int number = 0;
        System.out.println(Arrays.toString(numbers));
        Arrays.sort(numbers);
        int nearestUp = nearestUp(numbers,number,0,numbers.length-1);                          
        System.out.println(Arrays.toString(numbers));
        System.out.println("NearestUp: " + nearestUp);
    }

    public static int nearestUp(int array[], int number, int left, int right){
        int pivot = (left + right )/2;
        int central = array[pivot];
        int i = pivot;
        if(central < number && i <= right){  
            System.out.format("central:%d pivot:%d\n",central,pivot);          
            return nearestUp(array,number,++i,array.length-1);
        }       
        if(central == number && i < right){
            System.out.format("central:%d pivot:%d\n",central,pivot);          
            return array[pivot + 1];
        }
        if(central > number && i >= left){
            System.out.println("number: " + central);                      
            return nearestUp(array,number,left,--i);            
        }

        return number;
    }
}