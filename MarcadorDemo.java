public class MarcadorDemo{
    public static void main(String[] args) {
        Marcador m = new Marcador();

        System.out.println("Color:" + m.getColor());
        System.out.println("Tamano: " + m.getTamano());
        System.out.println("Tipo: " + m.getTipo());
        System.out.println("Marca: " + m.getMarca());
    }
}