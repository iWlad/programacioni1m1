/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion1.console;

/**
 *
 * @author DocenteFCyS
 */
public class ConsoleMenus {
    public static void mainMenu(){
        System.out.println("Menu principal");
        System.out.println("1. Gestion Empleado");
        System.out.println("2. Reportes");
        System.out.println("3. Salir");
    }
    
    public static void gestionEmpleadoMenu(){
        System.out.println("***Gestion Empleado***");
        System.out.println("1. Agregar");
        System.out.println("2. Editar");
        System.out.println("3. Eliminar");
        System.out.println("4. Regresar");
    }
    
    public static void reportesMenu(){
        System.out.println("***Reportes Empleado***");
        System.out.println("1. Visualizar todos");
        System.out.println("2. Visualizar por Sexo");
        System.out.println("3. Visualizar por Nivel academico");
        System.out.println("4. Visualizar por Municipio");
        System.out.println("5. Buscar por Cedula");
        System.out.println("6. Buscar por Apellido");
        System.out.println("7. Regresar");
    }
}
