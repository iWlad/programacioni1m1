/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion1.console;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Arrays;
import ni.edu.uni.programacion1.data.DepartamentoData;
import ni.edu.uni.programacion1.data.EmpleadoData;
import ni.edu.uni.programacion1.data.MunicipioData;
import ni.edu.uni.programacion1.enums.NivelAcademico;
import ni.edu.uni.programacion1.enums.Sexo;
import ni.edu.uni.programacion1.pojo.Departamento;
import ni.edu.uni.programacion1.pojo.Empleado;
import ni.edu.uni.programacion1.pojo.Municipio;

/**
 *
 * @author DocenteFCyS
 */
public class ConsoleGestionEmpleado {
    public static EmpleadoData EDATA ;
    public static DepartamentoData DDATA;
    public static MunicipioData MDATA;
    
    public static void agregarEmpleado(BufferedReader reader) throws IOException{
        int codigo = ConsoleReader.readInt(reader, "Codigo: ");
        String cedula = ConsoleReader.readString(reader, "Cedula: ");
        String name1 = ConsoleReader.readString(reader, "Primer Nombre: ");
        String name2 = ConsoleReader.readString(reader, "Segundo Nombre: ");
        String lastname1 = ConsoleReader.readString(reader, "Primer Apellido: ");
        String lastname2 = ConsoleReader.readString(reader, "Segundo Apellido: ");
        String address = ConsoleReader.readString(reader, "Direccion: ");
        String phoneNumber = ConsoleReader.readString(reader, "Telefono: ");
        String mobileNumber = ConsoleReader.readString(reader, "Celular: ");
        
        int sexoId;
        do{
            sexoId = ConsoleReader.readInt(reader, 
                    "Seleccione un sexo entre 1 - 2\n" + 
                    Arrays.toString(Sexo.values()) + ".\nOpcion: ");            
        }while(sexoId <=0 || sexoId > 2);
        Sexo sexo = Sexo.values()[sexoId - 1];
        
        int naId;
        do{
            naId = ConsoleReader.readInt(reader, 
                    "Seleccion un Nivel Academico entre 1 - 11\n" + 
                            Arrays.toString(NivelAcademico.values()) +
                            ".\nOpcion: ");
        }while(naId <= 0 || naId > 11);
        NivelAcademico na = NivelAcademico.values()[naId - 1];
        
        int dId;
        do{
            dId = ConsoleReader.readInt(reader, 
                    "Seleccione un departamento entre 1 - 17\n" + 
                            Arrays.toString(DDATA.getNameDepartamentos())+
                            "\nOpcion: ");
        }while(dId <= 0 || dId > 17);
        Departamento d = DDATA.getDepartamentoById(dId);
        
        
        String[] namesMunicipios = MDATA.getNamesMunicipios(d);
        int mId, n = namesMunicipios.length;
        do{
            mId = ConsoleReader.readInt(reader, 
                    "Seleccione un municipio entre 1 - "+
                            n + "\n" + 
                            Arrays.toString(namesMunicipios)+
                            "\nOpcion: ");
        }while(mId <= 0 || mId > n);
        Municipio m = MDATA.getMunicipioById(mId);
        
        Empleado e = new Empleado(codigo, cedula, name1, name2, lastname1, lastname2, address, phoneNumber, mobileNumber, sexo, na, m);
        EDATA.add(e);
    }
    
    public static void editarEmpleado(BufferedReader reader){
        
    }
    
    public static void eliminarEmpleado(BufferedReader reader){
        
    }
}
