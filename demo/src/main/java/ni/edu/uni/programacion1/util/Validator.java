/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion1.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author DocenteFCyS
 */
public class Validator {
    public static boolean validateIsInt(String value){        
        return validate("^\\d+$",value);
    }
    
    public static boolean validateIsLetter(String value){
        return validate("(?i)[a-z]([- ',.a-z]{0,23}[a-z])?",value);
    }
    
    public static boolean validateIsPhone(String value){
        return validate("\\d{10}|(?:\\d{3}-){2}\\d{4}|\\(\\d{3}\\)\\d{3}-?\\d{4}",value);
    }
    
    public static boolean validateIsCedula(String value){
        return validate("",value);
    }
    private static boolean validate(String pattern, String text){
        Pattern patron = Pattern.compile(pattern);
        Matcher match = patron.matcher(text);
        return match.matches();
    }
}
