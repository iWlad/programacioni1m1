/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion1.data;

import java.util.Comparator;
import ni.edu.uni.programacion1.pojo.Empleado;

/**
 *
 * @author DocenteFCyS
 */
public class EmpleadoCedulaComparator implements Comparator<Empleado> {

    @Override
    public int compare(Empleado arg0, Empleado arg1) {
        return arg0.getCedula().compareTo(arg1.getCedula());
    }
    
}
