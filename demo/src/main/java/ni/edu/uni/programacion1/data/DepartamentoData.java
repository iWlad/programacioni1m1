/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion1.data;

import ni.edu.uni.programacion1.pojo.Departamento;

/**
 *
 * @author DocenteFCyS
 */
public class DepartamentoData {

    private Departamento departamentos[];

    public DepartamentoData() {
        populateDepartamento();
    }

    private void populateDepartamento() {
        departamentos = new Departamento[]{
            new Departamento(1, "Boaco", "BO"),
            new Departamento(2, "Carazo", "CZ"),
            new Departamento(3, "Chinandega", "CH"),
            new Departamento(4, "Chontales", "CT"),
            new Departamento(5, "Costa Caribe Norte", "RN"),
            new Departamento(6, "Costa Caribe Sur", "RS"),
            new Departamento(7, "Esteli", "ES"),
            new Departamento(8, "Granada", "GR"),
            new Departamento(9, "Jinotega", "JI"),
            new Departamento(10, "Leon", "LE"),
            new Departamento(11, "Madriz", "MZ"),
            new Departamento(12, "Managua", "M"),
            new Departamento(13, "Masaya", "MY"),
            new Departamento(14, "Matagalpa", "MT"),
            new Departamento(15, "Nueva Segovia", "NS"),
            new Departamento(16, "Rio San Juan", "RJ"),
            new Departamento(17, "Rivas", "RI"),};
    }

    public Departamento[] getDepartamento() {
        return this.departamentos;
    }

    public Departamento getDepartamentoById(int id) {
        return (id <= 0) ? null
                : (id > departamentos.length) ? null
                        : departamentos[id - 1];
    }

    public Departamento getDepartamentoByName(String name){
        if(name == null){
            return null;
        }        
        if(name.isEmpty() || name.isBlank()){
            return null;
        }        
        int index = departamentoBinarySearch(name);        
        if(index < 0){
            return null;
        }        
        return departamentos[index];
    }
    private int departamentoBinarySearch(String name) {
        int index = -1;
        int low = 0, high = departamentos.length - 1;
        while (low <= high) {
            int mid = (low + high) / 2;
            if (departamentos[mid].getNombre()
                    .compareToIgnoreCase(name) < 0) {
                low = mid + 1;
            } else if (departamentos[mid].getNombre()
                    .compareToIgnoreCase(name) > 0) {
                high = mid - 1;
            } else if (departamentos[mid].getNombre()
                    .compareToIgnoreCase(name) == 0) {
                index = mid;
                break;
            }
        }
        return index;
    }
    
    public String[] getNameDepartamentos(){
        String[] names = new String[17];
        int i = 0;
        for(Departamento d : departamentos){
            names[i++] = d.getNombre();
        }
        
        return names;
    }
}
