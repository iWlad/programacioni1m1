/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion1.data;

import java.util.Arrays;
import ni.edu.uni.programacion1.pojo.Departamento;
import ni.edu.uni.programacion1.pojo.Municipio;

/**
 *
 * @author DocenteFCyS
 */
public class MunicipioData {

    private Municipio municipios[];
    private DepartamentoData dData;

    public MunicipioData() {
        dData = new DepartamentoData();
        populateMunicipios();
    }

    private void populateMunicipios() {
        municipios = new Municipio[]{
            new Municipio(1, "Boaco", dData.getDepartamentoById(1)),
            new Municipio(2, "Camoapa", dData.getDepartamentoById(1)),
            new Municipio(3, "San Lorenzo", dData.getDepartamentoById(1)),
            new Municipio(4, "San Jose de los Remates", dData.getDepartamentoById(1)),
            new Municipio(5, "Santa Lucia", dData.getDepartamentoById(1)),
            new Municipio(6, "Teustepe", dData.getDepartamentoById(1))
        };
    }
    
    private Municipio[] getMunicipios(){
        return this.municipios;
    }
    
    public Municipio getMunicipioById(int id){
        return (id <= 0) ? null : 
               (id > municipios.length) ? null : municipios[id - 1];
    }
    
    public Municipio[] getMunicipioByDepartamento(Departamento d){
        Municipio municipiosByDepartamento[] = null;
        
        for(Municipio m : this.municipios){
            if(m.getDepartamento().getCodigo() == d.getCodigo()){
                municipiosByDepartamento = 
                        addMunicipio(municipiosByDepartamento, m);
            }
        }
        return municipiosByDepartamento;
    }
    
    private Municipio[] addMunicipio(Municipio[] municipioCopy, Municipio m){
        if(municipioCopy == null){
            municipioCopy = new Municipio[1];
            municipioCopy[municipioCopy.length - 1] = m;
            return municipioCopy;
        }
        
        municipioCopy = Arrays.copyOf(municipioCopy, municipioCopy.length + 1);
        municipioCopy[municipioCopy.length - 1] = m;
        
        return municipioCopy;
    }
    
    public String[] getNamesMunicipios(Departamento d){
        Municipio[] mun = getMunicipioByDepartamento(d);
        String[] names = new String[mun.length];
        int i = 0;
        for(Municipio m : mun){
            names[i++] = m.getNombre();
        }
        
        return names;
    }
}
