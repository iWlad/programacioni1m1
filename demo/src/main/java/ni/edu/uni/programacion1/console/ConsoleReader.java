/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion1.console;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import ni.edu.uni.programacion1.util.Validator;

/**
 *
 * @author DocenteFCyS
 */
public class ConsoleReader {
    
    public static int readInt(BufferedReader reader, String message) throws IOException{        
        String text = "";
        do{
            System.out.print(message);
            text = reader.readLine();
        }while(!Validator.validateIsInt(text));
        
        return Integer.parseInt(text);
    }
    
    public static String readString(BufferedReader reader, String message) throws IOException{
        System.out.print(message);
        return reader.readLine();
    }
    
    
    /*
        TODO
        LEER CEDULA Y VALIDAR
        LEER NOMBRES Y APELLIDOS Y VALIDAR
        LEER TELEFONOS Y VALIDAR
        LEER CORREO Y VALIDAR
        LEER SEXO
        LEER NIVEL ACADEMICO
    
    */
    
}
