/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion1.console;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import ni.edu.uni.programacion1.pojo.Empleado;

/**
 *
 * @author DocenteFCyS
 */
public class ConsoleDemo {
    
    public static void start() throws IOException{
        boolean flag = true;
        int mainMenu = 0;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        do{
            ConsoleMenus.mainMenu();
            System.out.print("Opcion: ");
            mainMenu = Integer.parseInt(reader.readLine());
            
            switch(mainMenu){
                case 1:
                    gestionEmpleado(reader);
                    break;
                case 2:
                    reportesEmpleado(reader);
                    break;
                case 3:
                    flag = false;
                    System.out.println("Hasta la vista ...");
                    break;
                default:
                    System.err.println("Opcion seleccionada no valida!");
            }
        }while(flag);
    }
    
    public static void gestionEmpleado(BufferedReader reader) throws IOException{
        int opc = 0;
        boolean flag = true;
        do{
            ConsoleMenus.gestionEmpleadoMenu();
            System.out.print("Opcion: ");
            opc = Integer.parseInt(reader.readLine());
            switch(opc){
                case 1:
                    ConsoleGestionEmpleado.agregarEmpleado(reader);
                    break;
                case 2:
                    ConsoleGestionEmpleado.editarEmpleado(reader);
                    break;
                case 3:
                    ConsoleGestionEmpleado.eliminarEmpleado(reader);
                    break;
                case 4:
                default:
            }
        }while(flag);
    }
    
    public static void reportesEmpleado(BufferedReader reader){
        /*TODO
            Escribir codigo que gestione los reportes del empleado
            todas las visualizaciones
        */
    }
}
