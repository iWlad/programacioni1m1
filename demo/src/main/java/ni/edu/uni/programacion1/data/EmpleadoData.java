/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion1.data;

import java.util.Arrays;
import java.util.Comparator;
import ni.edu.uni.programacion1.pojo.Empleado;

/**
 *
 * @author DocenteFCyS
 */
public class EmpleadoData {

    private Empleado[] empleados;
    private MunicipioData mData;

    public EmpleadoData() {
        mData = new MunicipioData();
    }

    public void add(Empleado e) {
        empleados = addEmpleado(empleados, e);
    }

    private Empleado[] addEmpleado(Empleado[] ecopy, Empleado e) {
        if (ecopy == null) {
            ecopy = new Empleado[1];
            ecopy[ecopy.length - 1] = e;
            return ecopy;
        }

        ecopy = Arrays.copyOf(ecopy, ecopy.length + 1);
        ecopy[ecopy.length - 1] = e;

        return ecopy;
    }

    public Empleado[] getEmpleados() {
        return this.empleados;
    }

    public Empleado getEmpleadoByCodigo(int codigo) {
        if (codigo <= 0) {
            return null;
        }
        Empleado e = new Empleado();
        e.setCodigo(codigo);
        Comparator<Empleado> c = new Comparator<Empleado>() {
            @Override
            public int compare(Empleado arg0, Empleado arg1) {
                return (arg0.getCodigo() - arg1.getCodigo());
            }

        };
        Arrays.sort(empleados, c);
        int index = Arrays.binarySearch(empleados, e, c);
        if (index < 0) {
            return null;
        }

        return empleados[index];
    }

    public Empleado getEmpleadoByCedula(String cedula) {
        if (cedula == null) {
            return null;
        }
        if (cedula.isBlank() || cedula.isEmpty()) {
            return null;
        }
        EmpleadoCedulaComparator c = new EmpleadoCedulaComparator();
        Arrays.sort(empleados, c);
        Empleado e = new Empleado();
        e.setCedula(cedula);
        int index = Arrays.binarySearch(empleados, e, c);
        if(index < 0){
            return null;
        }
        
        return empleados[index];
    }

    /*
    TODO
        Empleados por Sexo
        Empleados por Nivel Academico
        Empleados por Municipio
        Empleados por Departamento
        
        Empleados por Apellido
        
     */
}
